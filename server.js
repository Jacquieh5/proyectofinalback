var express = require('express'),
  app = express(),
  port = process.env.PORT || 3002;

var path = require('path');

app.listen(port);

var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
})

var requestjson = require("request-json");
var urlMLabRaiz = "https://api.mlab.com/api/1/databases/phiguera/collections"
var apiKeyMLab  = "apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt"
var clienteMLabRaiz;

console.log('todo list RESTful API server started on: ' + port);

app.post('/login', function(req, res) {
   res.set("Access-Control-Allow-Headers", "Content-Type")
   var email = req.body.email
   var password = req.body.password
   var query = 'q={"email":"'+email+'","password":"'+password+'"}';
   console.log(query)
   clienteMLabRaiz = requestjson.createClient(urlMLabRaiz + "/Usuarios?" + apiKeyMLab + "&" + query)
   console.log(urlMLabRaiz + "/Usuarios?" + apiKeyMLab + "&" + query)
   clienteMLabRaiz.get('', function(err, resM, body) {
     if (!err){
       if (body.length == 1){ //login ok
         res.status(200).send({"resultado": "Usuario logado", "nombreVendedor": body[0].name, "idUsuario": body[0].idusuario})
       } else {
         res.status(400).send('Usuario no encontrado')
       }
     }
   })
 })


//Consultar DATOS
app.get('/Usuarios/:idusuario', function(req, res) {
  res.set("Access-Control-Allow-Headers", "Content-Type");
  var idUsuario = req.params.idusuario;
  var query = 'q={"idusuario":'+idUsuario+'}';
  clienteMLabRaiz = requestjson.createClient(urlMLabRaiz + "/Usuarios?" + apiKeyMLab + "&" + query);
  clienteMLabRaiz.get('', function(err, resM, body) {
    if (!err) {
      if (body.length >= 1) { //Se encontró el cliente
        res.status(200).send(body[0]);
      } else { //No se encontró al cliente
        res.status(204).send();
      }
    } else { //Ocurrió un error al hacer la consulta
      res.status(400).send({'error':'Error al hacer consulta.'});
    }
  })
});

app.get('/Productos', function(req, res) {
  res.set("Access-Control-Allow-Headers", "Content-Type");
  clienteMLabRaiz = requestjson.createClient(urlMLabRaiz + "/Productos?" + apiKeyMLab);
  clienteMLabRaiz.get('', function(err, resM, body) {
    if (!err) {
      if (body.length >= 1) { //Se encontró el cliente
        res.status(200).send(body);
      } else { //No se encontró al cliente
        res.status(204).send();
      }
    } else { //Ocurrió un error al hacer la consulta
      res.status(400).send({'error':'Error al hacer consulta.'});
    }
  })
});


//Consultar movimientos
app.get('/Usuarios/:idUsuario/Movimientos', function(req, res) {
  res.set("Access-Control-Allow-Headers", "Content-Type");
  var idUsuario = req.params.idUsuario;
  var query = 'q={"idusuario":'+idUsuario+'}';
  var fields = 'f={"movimientos": 1, "_id": 0}';
  clienteMLabRaiz = requestjson.createClient(urlMLabRaiz + "/Usuarios?" + apiKeyMLab + "&" + query + "&" + fields);
  clienteMLabRaiz.get('', function(err, resM, body) {
    if (!err) {
      if (body.length >= 1) { //Se encontraron movimientos
        res.status(200).send(body[0]);
      } else { //No se encontraron movimientos
        res.status(204).send();
      }
    } else { //Ocurrió un error al hacer la consulta
      res.status(400).send({'error':'Error al consultar movimientos.'});
    }
  })
});


//Registrar nuevo movimiento
app.post('/Usuarios/:idUsuario/Movimientos', function(req, res) {
  res.set("Access-Control-Allow-Headers", "Content-Type");
  var idUsuario = req.params.idUsuario;
  console.log(req.body.importeMov + " " +req.body.nombreProducto);
  var puntosProducto = req.body.importeMov;
  var nombre = req.body.nombreProducto;
  var puntosVendedor = req.body.puntos;
  var data = {
    $push: {
      movimientos:{
        "nombre": nombre,
        "puntos": puntosProducto
      }
  }};
  var data2 = {
    $set: {
      "puntos": puntosVendedor
    }
  };
  var data3 = {
    $inc:{
      "stock": -1
    }
  };
  console.log(JSON.stringify(data3));
  var query = 'q={"idusuario":'+idUsuario+'}';
  clienteMLabRaiz = requestjson.createClient(urlMLabRaiz + "/Usuarios?" + apiKeyMLab + "&" + query);
  clienteMLabRaiz.put('', data, function(err, resM, body) {
    if (resM.statusCode==200) {
      actualizaPuntosV();
    } else {
      console.log("Aqui 1");
      res.status(400).send({'error':'Error al registrar el movimiento'});
    }
  })

  function actualizaPuntosV() {
    clienteMLabRaiz = requestjson.createClient(urlMLabRaiz + "/Usuarios?" + apiKeyMLab + "&" + query);
    clienteMLabRaiz.put('', data2, function(err, resM, body) {
      if (resM.statusCode==200) {
        actualizaStock();
      } else {
          console.log("Aqui 2");
        res.status(400).send({'error':'Error al registrar el movimiento'});
      }
    })
  }
  function actualizaStock() {
    query = 'q={"name":"'+nombre+'"}';
    clienteMLabRaiz = requestjson.createClient(urlMLabRaiz + "/Productos?" + apiKeyMLab + "&" + query);
    console.log(clienteMLabRaiz);
    clienteMLabRaiz.put('', data3, function(err, resM, body) {
      if (resM.statusCode==200) {
        res.status(200).send("Se ha actualizado el movimiento");
      } else {
          console.log("Aqui 3");
        res.status(400).send({'error':'Error al registrar el movimiento'});
      }
    })
  }
});



//Obtener lista de productos
app.get('/Productos', function(req, res) {
  res.set("Access-Control-Allow-Headers", "Content-Type")
  clienteMLabRaiz = requestjson.createClient(urlMLabRaiz + "/Productos");
  clienteMLabRaiz.get('', function(err, resM, body) {
    if (!err) {
      if (body.length >= 1) {
        res.status(200).send(body[0]);
      } else {
        res.status(204).send();
      }
    } else { //Ocurrió un error al hacer la consulta
      console.log(err);
      res.status(400).send({'error':'Error al hacer consulta.'});
    }
  })
});
